using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadPlayerName : MonoBehaviour
{
    private GameObject _playerName;

    //private PlayerData _playerData;
    //private GameObject _player;

    void Start()
    {
        _playerName = GameObject.FindGameObjectWithTag("PlayerName");
        _playerName.GetComponent<Text>().text = PlayerPrefs.GetString("PlayerName");

        //_playerData = _player.GetComponent<PlayerData>();
        //_playerData.PlayerName = this.gameObject.GetComponent<Text>().text;
    }
}
