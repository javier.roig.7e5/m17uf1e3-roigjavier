using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NameSet : MonoBehaviour
{
    private GameObject _playerName;
    void Start()
    {
        _playerName = GameObject.Find("PlayerName");
        DontDestroyOnLoad(_playerName);
    }

    void Update()
    {
        _playerName.GetComponent<Text>().text = GameObject.Find("TextName").GetComponent<Text>().text;
    }
}