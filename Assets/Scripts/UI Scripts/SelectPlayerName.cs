using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectPlayerName : MonoBehaviour
{
    public InputField InputText;
    public Text PlayerName;
    public GameObject StartButton;
    public string NewScene;

    public void GameStart()
    {
        PlayerPrefs.SetString("PlayerName", InputText.text);
        SceneManager.LoadScene(NewScene);
    }
}
