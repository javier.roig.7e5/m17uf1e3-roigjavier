using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScreenDataRole : MonoBehaviour
{
    //Method 1
    private PlayerData _playerData;
    //Method 2
    private GameObject _player;
    // Start is called before the first frame update
    void Start()
    {
        //Method 2
        _player = GameObject.Find("Female_01");
        _playerData = _player.GetComponent<PlayerData>();
        
        //Method 1
        this.gameObject.GetComponent<Text>().text = Convert.ToString(_playerData.Player_role);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
