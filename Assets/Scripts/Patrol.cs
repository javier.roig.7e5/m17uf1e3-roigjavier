using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float speed;
    private float speed2;
    public float distance;
    public bool Patroling;

    private PlayerData _playerData;
    private GameObject _player;

    private bool movingLeft = true;

    public Transform graundDetection;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Female_01");
        _playerData = _player.GetComponent<PlayerData>();
        speed2 = speed;
        
    }

    // Update is called once per frame
    void Update()
    {
        speed = speed2;
        WeightOverSpeed();
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        RaycastHit2D groundInfo = Physics2D.Raycast(graundDetection.position, Vector2.down, distance);
        if (groundInfo.collider == false)
        {
            if (movingLeft == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = true;
            }
        }
    }

    void WeightOverSpeed()
    {
        float movVelocity = (speed * _playerData.Weight) / 100; 
        if (_playerData.Weight > 0 && _playerData.Weight < 101)
        {
            speed = speed - movVelocity;
            Debug.Log("Perc vel: " + movVelocity);
        }else if (_playerData.Weight >= 101)
        {
            speed = 0;
        }
    }
}
