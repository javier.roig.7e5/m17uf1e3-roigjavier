using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerRoles
{
    Priest,
    Warrior,
    Rogue,
    Mage,
    Hunter,
    Druid
}

public class PlayerData : MonoBehaviour
{
    public PlayerRoles Player_role;
    private string PlayerName;
    public float Height = 0.5f;
    public float Weight;
    public int stats;
    [SerializeField]
    private int _yearsOld = 20;
    [SerializeField]
    private Sprite _imatgeDelFrame01;
    public Transform MyTransform;
    public Sprite [] SpritesChange;
    private SpriteRenderer _sr;
    private int _frameIndex = 0;
    private int _counter;
    [SerializeField]
    public int FrameRate;
    //private float _speedAtenuator = 0.1f;

    //private GameObject _playerName;

    /*public float speed = 10.0f;
    public float rotationSpeed = 100.0f;*/


    void Start()
    {
        //_playerName = GameObject.FindGameObjectWithTag("PlayerName");
        PlayerName = PlayerPrefs.GetString("PlayerName");
        Debug.Log("Esto es un nombre: " + PlayerName);
        //PlayerName = GameObject.Find("PlayerName").GetComponent<Text>().text;
        switch (Player_role)
        {
            case PlayerRoles.Priest:
                Debug.Log("Hola soy un Priest");
                break;
            case PlayerRoles.Warrior:
                Debug.Log("Hola soy un Warrior");
                break;
            case PlayerRoles.Rogue:
                Debug.Log("Hola soy un Rogue");
                break;
            case PlayerRoles.Mage:
                Debug.Log("Hola soy un Mage");
                break;
            case PlayerRoles.Hunter:
                Debug.Log("Hola soy un Hunter");
                break;
            case PlayerRoles.Druid:
                Debug.Log("Hola soy un Druida");
                break;
        }
        //stats = new int[2];
        Debug.Log(Height);
        Debug.Log(_yearsOld);
        //Debug.Log("posicio x: " + transform.position.x);
        Debug.Log("rotacio x: " + transform.rotation);
        Debug.Log(_imatgeDelFrame01);
        Debug.Log(this.gameObject.GetComponent<SpriteRenderer>().sprite.name);
        _sr = GetComponent<SpriteRenderer>();
        Debug.Log(_sr.sprite.name);
        _counter = 0;
        //FrameRate = 120;
        //DontDestroyOnLoad(this.gameObject);

    }

    void Update()
    {
        if (_counter % FrameRate == 0)
        {
            _sr.sprite = SpritesChange[_frameIndex];
            _counter = 0;
            _frameIndex = _frameIndex < SpritesChange.Length - 1 ? (_frameIndex + 1) : 0;
        }
        _counter++;
    }

    public string GetPlayerName()
    {
        return PlayerName;
    }

}
