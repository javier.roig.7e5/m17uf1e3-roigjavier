using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformCollider : MonoBehaviour
{
    private float _sizeX = 10, _sizeY = 6;
    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<BoxCollider2D>().size = new Vector2(_sizeX, _sizeY);
    }

    // Update is called once per frame
    void Update()
    {
        if (_sizeX >= 0 && _sizeX <= 20 && _sizeY >= 3 && _sizeY <= 6 )
        {
            GetComponent<BoxCollider2D>().size = new Vector2(_sizeX, _sizeY);
        }
        
    }
}
