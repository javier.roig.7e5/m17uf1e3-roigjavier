using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameShow : MonoBehaviour
{
    private GameObject _namePlayer;
    private GameObject _rolePlayer;
    private PlayerData _playerData;
    private GameObject _nameText;
    //Method 2
    private GameObject _player;
    // Start is called before the first frame update
    void Start()
    {
        _namePlayer = GameObject.Find("Name");
        _rolePlayer = GameObject.Find("Role");
        _player = GameObject.Find("Female_01");
        _playerData = _player.GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.GetComponent<Text>().text = _playerData.GetPlayerName();
    }
}
